#!/usr/bin/env bash

ver="20230522"
url="https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/external/parallel/-/raw/${ver}/src"
echo "Installing GNU parallel ${ver}" 
mkdir local.bin
curl -SsL -o local.bin/parallel ${url}/parallel && chmod +x local.bin/parallel
curl -SsL -o local.bin/env_parallel.bash ${url}/env_parallel.bash && chmod +x local.bin/env_parallel.bash
