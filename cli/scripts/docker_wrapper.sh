#!/usr/bin/env bash
exec=$(basename ${BASH_SOURCE})
execdir=$(dirname ${BASH_SOURCE})
domain=${USER}-$(basename ${execdir})
exec docker compose -f instance/${domain}/docker-compose.yml --env-file instance/${domain}/.env  exec api bash --login -c "${exec} ${@:2}"
