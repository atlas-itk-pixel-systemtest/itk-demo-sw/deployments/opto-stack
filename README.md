# Containerized Optoboard Software Stacks

This repo contains a command line tool `opto-stack` to manage microservices containers
required to configure an OptoBoard:

- FELIX software container
- OptoBoard software container

optional:

- DeMi Service registry
- DeMi Dashboard
- Portainer

Below we describe various methods to operate the opto-stack.

## Method 1:  Use the opto-stack CLI from your local shell

<!-- This method does not require to clone this repo. -->

Requirements:

- Working Docker installation.
- git installed
<!-- - cURL installed (alternative: wget). -->

This will run the opto-stack CLI in a container so you do not need any of it's dependencies installed locally.
The configuration files and instance files are kept on the host file system.
They persist between runs of the opto-stack.

<!-- 1. Create working directory and download provided `opto-stack` script. -->
### 1. Clone opto-stack repository:

<!-- ```shell
mkdir opto-stack
cd opto-stack
curl -O https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/opto-stack/-/raw/master/opto-stack
``` -->
```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/opto-stack.git
cd opto-stack/
```

### 2. Optional: Confirm containerized opto-stack CLI is functional:

```shell
./opto-stack check
```

Note: You can set the verbosity of the opto-stack commands using an environment variable called VERBOSE.
```shell
export VERBOSE=1
# 1: Additional output from the commands executed inside of the opto-stack docker container
# 2: Additional output from bringing up the opto-stack docker container
```

### 3. Optional: Specify which microservices/tool to bring up:

Edit the cli/repos.list file to specify which microservices/tools should be included in your opto-stack. To remove a specific microservice simply comment it out using the # symbol.

<!-- 3. Create the top-level configuration file.

   [[TO DO]] -->

### 4. Apply the configuration:

This creates configured docker compose files that are used to bring up the docker containers. These instances are saved in the /instance directory.
```shell
./opto-stack config
```

### 5. Provide flx.cfg file:

In order for the felix microservice to be able to connect to your felix, you need to provide a felix config.
Simply copy it into the following path for it to be available to the felix container:
pwd/instance/itk-demo-felix/config/flx.cfg

### 6. Pull and start the containers:

```shell
./opto-stack up
```

### 7. Use the containers:

If you included the dashboard in the list of microservices it should be accessible via localhost in your browser. It will list all the other tools and microservices that are running on your PC.
You can also check which containers are running with 'docker ps'.

To use for example the cli provided by the microservices one has to navigate into the instance directory of this microservice. There you can execute commands inside of the containers using docker compose exec.
E.g. for the felix-ms and the flx-info link command:
```shell
cd instance/opto_stack_itk-demo-felix-api/
docker compose exec api bash --login -c flx-info link
```
To simplify the access to the cli of the microservices shims were added. These allow you to execute the cli commands inside of the containers without the need to navigate into the instance directory and using docker compose exec.
These are not yet supported by the opto-stack, but will be in the future.


## Method 2: Use the opto-stack CLI from the container shell

Requirements:

- Same as Method 1

This is the preferred method if you don't want to modify any local files at all.
Configuration and instance files are kept in the opto-stack container.
They **do not** persist between recreations of the opto-stack container!

1. Obtain and test the opto-stack script as in Method 1.
1. Enter the opto-stack container

```shell
./opto-stack shell
```

3. Edit the configuration inside the container
3. Apply the configuration and start the containers

```shell
./opto-stack config
./opto-stack up
```

Exit the shell to `exit` the opto-stack container.
The opto-stack container uses a docker-from-docker setup.
This means:
Any containers started from the opto-stack shell will still run after exiting the opto-stack container, but the *configuration and instance data* wil be removed with the opto-stack container.

## Advanced Users and Developers

The methods in this section generally require to clone this repo first.

```shell
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/deployments/opto-stack.git
cd opto-stack/
```

### Working in the Devcontainer

A development container is provided in `.devcontainer`.
Best used with VSCode but plain Docker etc. works as well.

Running in the devcontainer is very similar to Method 2, but it has more development tools installed.
This method is recommened to develop on the opto-stack repo itself with the need to install development tools locally.

### Working natively

top-level scripts

- `opto-stack` opto-stack container wrapper script.
- `setup` [[TO DO]] install `opto-stack` to PATH.
- `tasks` general development and maintenance tasks.

`cli/`

- `opto-stack` opto-stack cli hook.
- `repos.list` configuration files: repositories to include in opto stack.

`./cli/libexec` opto-stack subcommand scripts live

- `bootstrap` check and install dependencies.
- `config` configure instances.
- `main` main cli script.

Working *without* opto-stack script

1. Run bootstrapping script to check (and install) dependencies.

```shell
./bootstrap
```

3. Pull reference repositories and configure instances.

```shell
./config
```

4. Pull the images from the CERN GitLab image registry.

```shell
./opto-stack pull
```

5. Start all opto-stack containers.

```shell
./opto-stack up
```

6. Bring everything down.

```shell
./bootstrap stacks down
```

7. Clean up.

```shell
./tasks clean
```
